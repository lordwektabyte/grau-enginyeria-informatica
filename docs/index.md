# Grau Enginyeria Informàtica

## Introducció

Aquest repositori conté documentació, notes i apunts relacionats amb el Grau en Enginyeria Informàtica (GEI) cursat a la Universitat Oberta de Cataluya (UOC).

La idea darrere de publicar aquest treball és per a tenir contingut d'aquest tipus a l’abast durant el curs amb la possibilitat de fer-lo públic per a ajudar altres estudiants o qualsevol persona a qui pugui interessar-li.

---

## Responsabilitats

- La informació continguda en aquest lloc web (el "Servei") només té finalitat d'informació general: en cap cas es fa responsable del que un pugui fer a partir de les documentacions, tutorials o notes presents en aquest espai web.

- L'autor no assumeix cap responsabilitat per errors o omissions en els continguts del Servei. Així mateix, tampoc es fa responsable de possibles actualitzacions del contingut.

- En cap cas, l'autor serà responsable de danys especials, directes, indirectes, conseqüents, incidentals o danys i perjudicis, ja sigui en una acció de contracte, negligència o un altre delicte, derivat o relacionat amb l'ús del Servei o dels continguts del Servei.

- L'autor es reserva el dret d’afegir, esborrar o modificar els continguts del Servei en qualsevol moment sense previ avís.

- L'autor no pot garantir que el lloc web estigui lliure de virus o altres components nocius.

## Sobre mi

Podeu conèixer més de mi i del que faig a [Linkedin](https://es.linkedin.com/in/guillemsboeck), <a rel="me" href="https://mastodont.cat/@lordwektabyte"> Mastodon</a> i [Twitter](https://twitter.com/LordWektabyte).
