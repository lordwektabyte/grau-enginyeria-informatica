# Depuració amb Kdbg

## Depurar programa

```bash
kdbg ./executable
```

## Valors de variables i registres

Podem veure el contingut de les variables, adreces de memòria i registres afegint _watches_ durant l'execució pas a pas d'un programa.

### Contingut d'una variable

`(mida) nom_variable`

!!! example "Exemple"

    `(char)`, `(char[8])`, `(short[4])`, `(int[2])`, `(long)`

### Adreça d'una variable (i contingut)

`&nom_variable`

### Contingut d'un registre de 64 bits:

`$nom_registre`

Per a veure només una part dels registres r8-r15 cal utilitzar un sufix segons el què volguem recuperar:

- `l`: 1 byte menys significatiu
- `w` (word): 2 bytes menys significatius
- `d` (double) 4 bytes menys significatius

Per als registres `rax`, `rbx`, `rcx` i `rdx` podem utilitzar:

- `al`, `bl`, `cl`, `dl`, : primer byte menys significatiu
- `ah`, `bh`, `ch`, `dh`, : segon byte menys significatiu

Per als registres `rsi`, `rdi` i `rbp` podem utilitzar:

- `sil`, `dil`, `bpl`, : primer byte menys significatiu

!!! example "Exemple"

    - `$al`  primer byte menys significatiu de `rax`
    - `$ah`  segon byte menys significatiu de `rax`
    - `$ax`  valor dels 2 bytes menys significatius de `rax`
    - `$eax` valor dels 4 byte menys significatiu de `rax`
    - `$rax` valor del `rax` sencer (8 bytes)
    - `$r8l` 1 byte menys significatiu de `r8`
    - `$r8d` 4 bytes menys significatius de `r8`
