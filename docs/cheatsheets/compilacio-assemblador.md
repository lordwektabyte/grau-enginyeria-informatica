# Compilació Assemblador

## Assemblatge codi font

Tenim un fitxer font `source.asm` i obtenim el fitxer objecte `source.o`.

```bash
yasm -f elf64 -g dwarf2 "source.asm"
```

!!! output

    Codi objecte de l'assemblador `source.o`.

## Generar executable d'assemblador

Compilem el fitxer `source.o` i obtenim l'executable `output`.

```bash
gcc --no-pie -o "output" "source.o"
```

!!! output

    Fitxer executable `output`.

## Compilar codi font en C i Assemblador

Primer generem `source.o` i després el compilem amb `source.c` i obtenim l'executable `output`.

```bash
yasm -f elf64 -g dwarf2 "source.asm"
gcc --no-pie -o "output" -g "source.o" "source.c"
```

!!! info

    Podem afegir tants fitxers `.o` i `.c` a compilar com necessitem per a generar l'executable del nostre programa.
